<thead>
    <tr>
        <?php if(have_rows('table_headers')): while(have_rows('table_headers')): the_row(); ?>

            <?php 
                $label = get_sub_field('header');
                $slug = sanitize_title_with_dashes($label);
            ?>

            <th class="<?php echo $slug; ?>"><?php echo $label; ?></th>
        
        <?php endwhile; endif; ?>

    </tr>
</thead>