<nav class="site-nav">
    <ul>
        <li><a class="home" href="<?php echo site_url('/'); ?>">Home</a></li>
        <li><a class="supplemental" href="<?php echo site_url('/supplemental/'); ?>">Record of Proceedings after March 8, 2022</a></li>
    </ul>
</nav>