<?php

/*
	Enqueue Styles & Scripts
*/

function esa_enqueue_child_styles_and_scripts() {
    // Add style.css and third-party css
    wp_enqueue_style( 'oakland-ballpark', get_stylesheet_directory_uri() . '/oakland-ballpark.css', '' );

    // Add plugins.js & site.js (with jQuery dependency)
    wp_enqueue_script( 'custom-plugins', get_template_directory_uri() . '/js/plugins.js', array( 'jquery.3.5.1' ));
    wp_enqueue_script( 'custom-site', get_template_directory_uri() . '/js/site.js', array( 'jquery.3.5.1' ));
}

add_action( 'wp_enqueue_scripts', 'esa_enqueue_child_styles_and_scripts', 11 );